"use strict";

const modalAdd = document.querySelector(".modal");
const addAd = document.querySelector(".add__ad");
const modalBtnSubmit = document.querySelector(".modal__btn-submit");
const modalBtnWarning = document.querySelector(".modal__btn-warning");
const modalSubmit = document.querySelector(".modal__submit");
const modalItem = document.querySelector(".modal__item");
const modalFileInput = document.querySelector(".modal__file-input");
const modalFileBtn = document.querySelector(".modal__file-btn");
const modalImageAdd = document.querySelector(".modal__image-add");
const catalog = document.querySelector(".catalog");
const searchInput = document.querySelector(".search__input");
const menuContainer = document.querySelector(".menu__container");
let selectedCategory = null;
let database = [];

searchInput.addEventListener("input", ({ target }) => {
    showCards();
});

Array.from(menuContainer.querySelectorAll("a")).forEach((el) => {
    el.addEventListener("click", ({ target }) => {
        selectedCategory = target.dataset.category;
        showCards();
    });
});

addAd.addEventListener("click", () => {
    modalAdd.classList.remove("hide");
    modalBtnSubmit.disabled = true;
});

catalog.addEventListener("click", ({ target }) => {
    let card = target.closest(".card");
    if (card) {
        let item = database[card.dataset.id];
        modalItem.classList.remove("hide");
        modalItem.innerHTML = `<div class="modal__block">
            <h2 class="modal__header">Купить</h2>
            <div class="modal__content">
                <div><img alt="test" class="modal__image modal__image-item" src="${ item.image }"></div>
                <div class="modal__description">
                    <h3 class="modal__header-item">${ item.nameItem }</h3>
                    <p>Состояние: <span class="modal__status-item">${ item.status === "new" ? "новое" : "б/у" }</span></p>
                    <p>Описание:
                        <span class="modal__description-item">${ item.descriptionItem }</span>
                    </p>
                    <p>Цена: <span class="modal__cost-item">${ item.costItem } ₽</span></p>
                    <button class="btn">Купить</button>
                </div>
            </div>
            <button class="modal__close">&#10008;</button>
        </div>`;
    }
});

modalAdd.addEventListener("click", closeModalWithMouse);
modalItem.addEventListener("click", closeModalWithMouse);

document.addEventListener("keydown", ({ key }) => {
    if (key !== "Escape")
        return;
    closeModal(modalAdd);
    closeModal(modalItem);
});

const elementsModalSumbit = Array.from(modalSubmit.querySelectorAll("input:not([type='submit']),select,textarea"));
elementsModalSumbit.forEach((input) => input.addEventListener("input", validateModalSubmit));
modalFileInput.addEventListener("change", e => {
    let file = e.target.files[0];
    if (file.size > 2 * 1024 * 1024) {
        alert("Файл слишком большой!");
        modalFileBtn.innerText = "Файл слишком большой";
        modalFileInput.value = null;
        validateModalSubmit();
        return;
    }
    if (file) {
        let reader = new FileReader();
        reader.onload = e => modalImageAdd.src = e.target.result;
        reader.readAsDataURL(file);
        modalFileBtn.innerText = file.name;
    }
});

modalSubmit.addEventListener("submit", e => {
    e.preventDefault();
    let data = elementsModalSumbit.reduce((res, el) => {
        res[el.name] = el.value;
        return res;
    }, {});


    let imageInput = modalFileInput.files[0];
    let reader = new FileReader();
    reader.onload = (e) => {
        data["image"] = e.target.result;
        closeModal(modalAdd);
        database.push(data);
        saveDB();
        console.log(data);
        showCards();
    };
    reader.readAsDataURL(imageInput);
});

function validateModalSubmit() {
    let validate = elementsModalSumbit.every((input) => input.value.trim());
    modalBtnWarning.classList.toggle("hide", validate);
    modalBtnSubmit.disabled = !validate;
}

function closeModalWithMouse({ target }) {
    if ((target.classList.contains("modal__close") || target === this))
        closeModal(this);
}

function closeModal(modal) {
    modal.classList.add("hide");
    modalSubmit.reset();
    modalFileBtn.innerText = "Добавить фото";
}

function saveDB() {
    localStorage.setItem("database", JSON.stringify(database));
}

function loadDB() {
    let save = localStorage.getItem("database");
    database = save ? JSON.parse(save) : [];
}

function showCards() {
    let filter = searchInput.value.trim().toLowerCase();

    catalog.innerHTML = "";
    database.forEach((item, index) => {
        if ((!filter || item.nameItem.toLowerCase().includes(filter) || item.descriptionItem.toLowerCase().includes(filter)) &&
            (!selectedCategory || selectedCategory === item.category)
        ) {
            catalog.insertAdjacentHTML("beforeend", `<li class="card" data-id="${ index }">
                <img alt="${ item.nameItem }" class="card__image" src="${ item.image }">
                <div class="card__description">
                    <h3 class="card__header">${ item.nameItem }</h3>
                    <div class="card__price">${ item.costItem } ₽</div>
                </div>
            </li>`);
        }
    });
}

loadDB();
showCards();